import { TokenStorageService } from './services/token-storage.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  constructor(private tokenStorageService: TokenStorageService) {}
  username: string = '';
  
  ngOnInit() {
    const user = this.tokenStorageService.getUser();
    this.username = user;
    
  }

  logout(): void {
    this.tokenStorageService.signOut();
    window.location.reload();
  }
}
