export const environment = {
  production: false,
  SOCKET_ENDPOINT: 'ws://80.91.189.243:19999',
  SERVER_ENDPOINT: 'http://80.91.189.243:19999'
  // SOCKET_ENDPOINT: 'ws://localhost:5000',
  // SERVER_ENDPOINT: 'http://localhost:5000'
};
