export interface IMessage {
  username: string;
  text: string;
  created_at: number;
}
