import { TokenStorageService } from './services/token-storage.service';
import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {

  constructor(private tokenStorageService: TokenStorageService) {

  }
  canActivate() {
    return !!this.tokenStorageService.getToken();
  }
}
