import { IMessage } from './../../interfaces/message.interface';
import {
  Component,
  Input,
} from '@angular/core';

@Component({
  selector: 'app-chat-window',
  templateUrl: './chat-window.component.html',
  styleUrls: ['./chat-window.component.css'],
})
export class ChatWindowComponent {
@Input() messages: IMessage[]
@Input() username: string
@Input() currentRoomId: number
  constructor() {}


}
