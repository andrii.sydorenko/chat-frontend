import { ChatService } from './../../services/chat.service';
import { Component, Input, OnInit } from '@angular/core';
import { IMessage } from 'src/app/interfaces/message.interface';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css'],
})
export class MessagesComponent implements OnInit {
  @Input() messages;
  @Input() username: string;
  constructor(private chatService: ChatService) {}

  ngOnInit(): void {}

  deleteMessage(id) {
    this.chatService.deleteMessage(id);
  }

  private isMessageYours(message: IMessage) {
    return message.username === this.username;
  }
}
