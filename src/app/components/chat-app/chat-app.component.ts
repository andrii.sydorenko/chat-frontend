import { IMessage } from './../../interfaces/message.interface';
import { TokenStorageService } from './../../services/token-storage.service';
import { ChatService } from './../../services/chat.service';
import { AfterContentInit, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-chat-app',
  templateUrl: './chat-app.component.html',
  styleUrls: ['./chat-app.component.css'],
})
export class ChatAppComponent implements OnInit, AfterContentInit {
  isLoggedIn: boolean = false;
  username: string;
  currentRoomId: number = 0;
  messages: IMessage[] = [];

  constructor(
    private chatService: ChatService,
    private tokenStorageService: TokenStorageService
  ) {}

  ngOnInit() {
    this.chatService.setupSocketConnection();

    if (this.tokenStorageService.getToken()) {
      this.isLoggedIn = true;
    }

    const user = this.tokenStorageService.getUser();
    this.username = user;

    this.getMessages(this.currentRoomId);
  }

  ngAfterContentInit() {
    this.chatService.messageListener().subscribe((messages) => {
      this.messages = messages.reverse();
    });
  }

  getMessages(roomId) {
    if (roomId != 0) {
      this.chatService.getMessages(roomId).subscribe((messages: IMessage[]) => {
        this.messages = messages['messages'];
        this.messages.reverse();
      });
    }
  }

  sendMessage(message) {
    if (this.currentRoomId != 0) {
      const newMessage: IMessage = {
        username: this.username,
        text: message,
        created_at: Date.now(),
      };
      this.chatService.sendMessage(newMessage, this.currentRoomId);
    }
  }

  deleteMessage(id) {
    console.log(id);
  }

  createRoom(name) {
    const room = { name, owner: this.username };
    this.chatService.createRoom(room);
  }

  joinRoom(roomId) {
    this.currentRoomId = roomId;
    this.getMessages(this.currentRoomId);
    this.chatService.joinRoom({
      username: this.username,
      roomId: this.currentRoomId,
    });
  }
}
