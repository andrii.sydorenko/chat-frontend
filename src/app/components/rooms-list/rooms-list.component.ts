import { ChatService } from './../../services/chat.service';
import { AfterContentInit, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-rooms-list',
  templateUrl: './rooms-list.component.html',
  styleUrls: ['./rooms-list.component.css'],
})
export class RoomsListComponent implements OnInit, AfterContentInit {
  @Input() username: string;
  @Input() currentRoomId: number;
  @Output() createRoomEvent = new EventEmitter<string>();
  @Output() joinRoomEvent = new EventEmitter<number>();
  rooms: any[] = [];

  room = new FormControl('', [
    Validators.required,
    Validators.maxLength(50)
  ]);

  constructor(private chatService: ChatService) {}

  ngOnInit(): void {
    this.chatService.getRooms().subscribe((data) => {
      this.rooms = data['rooms'];
    });
  }

  ngAfterContentInit() {
    this.chatService
      .roomListListener()
      .subscribe((data) => (this.rooms = data));
  }

  createRoom() {
    this.createRoomEvent.emit(this.room.value);
    this.room.reset();
  }

  updateRoomStatus(id, status) {
    
    this.chatService.updateRoomStatus({ id, is_opened: status})
  }

  joinRoom(roomId) {
    this.joinRoomEvent.emit(roomId);
  }

  private isRoomActive(room) {
    return room.id === this.currentRoomId
  }

  private isRoomOpened(room) {
    return room['is_opened']
  }
}
