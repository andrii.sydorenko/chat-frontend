import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-send-form',
  templateUrl: './send-form.component.html',
  styleUrls: ['./send-form.component.css'],
})
export class SendFormComponent implements OnInit {
  @Output() sendMessageEvent = new EventEmitter<string>();
  message = new FormControl('', [Validators.required]);

  constructor() {}

  ngOnInit(): void {}

  sendMessage() {
    this.sendMessageEvent.emit(this.message.value);
    this.message.reset();
  }
}
