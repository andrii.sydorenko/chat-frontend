import { IMessage } from './../interfaces/message.interface';
import { HttpClient } from '@angular/common/http';
import { Injectable, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { io } from 'socket.io-client';
import { environment } from '../environment';

@Injectable({
  providedIn: 'root',
})
export class ChatService implements OnInit {
  socket: any;
  constructor(private httpClient: HttpClient) {}

  ngOnInit() {
    this.setupSocketConnection();
  }

  setupSocketConnection() {
    this.socket = io(environment.SOCKET_ENDPOINT);
  }

  getMessages(roomId) {    
    return this.httpClient.get(
      environment.SERVER_ENDPOINT + '/messages/' + roomId
    );
  }

  sendMessage(message, roomId) {    
    this.socket.emit('chat message', message, roomId);
  }

  deleteMessage(messageId) {
    this.socket.emit('chat delete message', messageId)
  }

  getRooms() {
    return this.httpClient.get(environment.SERVER_ENDPOINT + '/rooms');
  }

  createRoom(room) {
    this.socket.emit('chat room', room);
  }

  updateRoomStatus(status) {
    this.socket.emit('chat room status', status);
  }

  messageListener() {
    return new Observable<IMessage[]>((observer) => {
      if (this.socket) {
        this.socket.on('chat message', (msgs) => {
          observer.next(msgs);
        });
      }
    });
  }

  roomListListener() {
    return new Observable<any[]>((observer) => {
      if (this.socket) {
        this.socket.on('chat room', (rooms) => {
          observer.next(rooms);
        });
      }
    });
  }

  joinRoom(data) {    
    this.socket.emit('join room', data);
  }
}
